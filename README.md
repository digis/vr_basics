# Basic VR sample (Oculus Quest 2)

### Video content
1) Project setup, connecting Quest with UE5 - basic VR template
2) Own Pawn creation, setting up HMD camera and basic controllers, difference between TrackingOrigin - Floor / Eye Level
3) Locomotion - Snap Turn
4) Teleportation - Template VRpawn analytics and funkcionality duplication
5) Grab - Template VRpawn analytics
6) Navigation in our scene, change to VRpawn
7) NavMesh generation - customizing navMesh
8) Dynamic navMeshes
### Personal Excersise
* 


### Resources
* Oculus Developer Hub (ODH): https://developer.oculus.com/downloads/unreal-engine/
* https://developer.oculus.com/documentation/unreal/unreal-plugin-settings/
* https://developer.oculus.com/documentation/unreal/unreal-engine-basics/